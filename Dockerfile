FROM python:3.6
WORKDIR /app
RUN pip install django djangorestframework Markdown django-filter gunicorn gevent
RUN apt-get update && apt-get -y install ffmpeg
COPY . /app
EXPOSE 5000
CMD gunicorn --workers 3 videoapi.wsgi --bind 0.0.0.0:5000