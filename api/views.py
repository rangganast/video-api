import os
import subprocess
from django.core.files.storage import default_storage
from django.http import FileResponse
from rest_framework import status, views
from rest_framework.decorators import api_view
from rest_framework.response import Response

from . import serializers

class VideoView(views.APIView):
    
    def post(self, request):
        serializer = serializers.VideoSerializer(data=request.data)

        if serializer.is_valid():
            video = request.data['video']
            output_format = request.data['output_format']

            saved_video_path = 'videos/{}'.format(video.name)
            saved_video = default_storage.save(saved_video_path, video)

            output = 'videos/{0}.{1}'.format(video.name.split('.')[0], output_format)
            subprocess.run(['ffmpeg', '-i', saved_video_path, output, '-y'])

            default_storage.delete(saved_video_path)

            print(os.stat(output).st_size)

            response = FileResponse(output, content_type='whatever')
            response['Content-Length'] = os.stat(output).st_size
            response['Content-Disposition'] = 'attachment; filename="%s"' % output

            return response

        return Response(serializer.errors, status=status.HTTP_406_NOT_ACCEPTABLE)