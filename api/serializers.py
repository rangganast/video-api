from rest_framework import serializers

class VideoSerializer(serializers.Serializer):
    VIDEO_FORMAT = [
        ('mp4', 'mp4'),
        ('mp3', 'mp3'),
        ('ts', 'ts'),
    ]

    video = serializers.FileField()
    output_format = serializers.ChoiceField(choices=VIDEO_FORMAT)