# Video API

Prosa.ai Interview Task

## Container Registry Notes
Run on port 5000

## How to Use API
URL: http://localhost:5000/api/video
METHOD: POST
Body:
    {
        'output_format' : 'mp4',
        'video': 'video-file.webm'
    }

output_format choices: mp4, webm, mp3